from setuptools import setup

setup(
    name='ii-jupyter-helpers',
    url='https://gitlab.com/infused-insight/libs/ii_jupyter_helpers',
    author='Kim Streich',
    author_email='kim@infusedinsight.com',
    packages=['ii_jupyter_helpers'],
    install_requires=[
        'numpy',
        'pandas',
        'ipykernel',
        'keyring',
    ],
    version='0.1',
    license='MIT',
    description='Commonly used functions when working with jupyter notebooks.',
)

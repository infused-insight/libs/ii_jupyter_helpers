'''
Provides commonly used functions when working with jupyter notebooks.

Use `from ii_jupyter_helpers import *` to make commonly used modules,
such as pd, and pprint available in your jupyter notebook.
'''

from getpass import getpass
import logging as log
import keyring

# Useful imports for notebooks
import pathlib  # noqa F401
import os  # noqa F401
import sys  # noqa F401
import pandas as pd  # noqa F401

from IPython.display import HTML  # noqa F401
from pprint import pprint  # noqa F401
from datetime import datetime  # noqa F401


def init_jupyter():
    '''
    Initializes jupyter and sets up logging.

    If the global variable `should_debug` is set to True, it will set the
    log level to DEBUG.
    '''
    debug = False
    if 'should_debug' in globals():
        debug = True
    enable_logging(debug)


def enable_logging(should_debug=False):
    '''
    Sets the log level to INFO and sets up the logging format.

    If `should_debug=True`, then it sets log level to DEBUG.
    '''
    log_level = log.INFO
    if should_debug is True:
        log_level = log.DEBUG

    log.basicConfig(
        format='%(asctime)s %(levelname)-8s %(message)s',
        level=log_level,
        datefmt='%Y-%m-%d %H:%M:%S'
    )


def store_secret(service, username=None):
    '''
    Stores a password in the macOS keychain or Windows Credential Manager.
    '''
    secret = getpass(f'Enter the secret for `{service}` / `{username}`:')

    service_full = f'jupyter_{service}'
    keyring.set_password(
        service_full,
        username,
        secret,
    )


def get_secret(service, username=None):
    '''
    Retrieves a password from the macOS keychain or Windows Credential Manager.

    Allows you to keep passwords outside of jupyter notebooks.
    '''
    service_full = f'jupyter_{service}'
    return keyring.get_password(
        service_full,
        username,
    )

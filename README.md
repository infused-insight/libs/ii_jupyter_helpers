# Infused Insight - Jupyter Helpers

## Description

Provides commonly used functions when working with jupyter notebooks.

## Install

To install the package, clone it and run `python setup.py install`.

Alternatively, you can also use pin and install directly from git... 

```shell
# Latest version
pip install git+https://gitlab.com/infused-insight/libs/ii_jupyter_helpers

# Specific version
pip install git+https://gitlab.com/infused-insight/libs/ii_jupyter_helpers@v0.1
```

## Usage

Check `ii_jupyter_helpers/__init__.py` for list of functions.

Use `from ii_jupyter_helpers import *` to make commonly used modules, such as pd, display and pprint available in your jupyter notebook.
